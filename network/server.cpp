#include "server.h"

Server::Server(QObject *parent) :
    QTcpServer(parent)
{

}

bool Server::startServer(quint16 port)
{
    return listen(QHostAddress::AnyIPv4, port);
}

void Server::stopServer()
{
    return close();
}


void Server::incomingConnection(int handle)
{
    ConnectionTCP * connection = new ConnectionTCP(handle, this);
}
