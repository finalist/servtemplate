#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>
#include "network/connectiontcp.h"

class Server : public QTcpServer
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = 0);
    bool startServer(quint16 port);
    void stopServer();
protected:
    void incomingConnection(int handle);
signals:

public slots:

};

#endif // SERVER_H
