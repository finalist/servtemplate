#include "connectiontcp.h"
#include <QDateTime>

ConnectionTCP::ConnectionTCP(int handle, QObject *parent) :
    QTcpSocket(parent), m_messageSize(0), m_SocketInited(false), m_bytesSend(0)
{
    connect(this, SIGNAL(readyRead()), SLOT(receiveData()));
    connect(this, SIGNAL(error(QAbstractSocket::SocketError)), SLOT(errorSocket(QAbstractSocket::SocketError)));
    connect(this, SIGNAL(disconnected()), SLOT(closeSocket()));

    bool ok = setSocketDescriptor(handle);
    qDebug() << this << "setSocketDescriptor" << ok;
    if (false == ok)
    {
        closeSocket();
    }
    m_stream.setDevice(this);
}

ConnectionTCP::~ConnectionTCP()
{
    qDebug() << "delete" << this;
}

void ConnectionTCP::receiveData()
{
    if (state() == ConnectedState)
    {
        if (m_messageSize == 0)
        {
            if(bytesAvailable() < MESSAGE_SIZE)
            {
                return;
            }
            m_stream >> m_messageSize;
        }
        if(bytesAvailable() < static_cast<qint64>(m_messageSize))
        {
            return;
        }

        QByteArray ba;
        unsigned char sym = 0;
        while (m_messageSize > 0)
        {
            m_stream >> sym;
            ba.append(sym);
            m_messageSize--;
        }

#ifdef USE_JSON
        QJsonDocument jsondocument = QJsonDocument::fromJson(ba);
        if (!jsondocument.isNull())
        {
            QVariantMap data = jsondocument.toVariant().toMap();

            if (!data.contains("p"))
            {
                qDebug() << QDateTime::currentDateTime().toString("hh:mm:ss") << "=>" << m_hash << ba;
            }

            if (isInited())
            {
                fromInput(data);
            }
            else
            {
                //auth(data);
            }

            if (bytesAvailable() > 0)
            {
                receiveData();
            }
        }
#endif
    }
}

void ConnectionTCP::closeSocket()
{
    deleteLater();
}

void ConnectionTCP::errorSocket(QAbstractSocket::SocketError)
{
    closeSocket();
}

void ConnectionTCP::send(const QVariantMap &data)
{
    if (state() == ConnectedState)
    {
        // QByteArray result - data to send
#ifdef USE_JSON
        QJsonDocument jsondocument = QJsonDocument::fromVariant(QVariant(data));
        QByteArray result = jsondocument.toJson(QJsonDocument::Compact);
#endif
        if (result.contains("\\\\"))
        {
            result = result.replace("\\\\", "\\");
        }
        if (!data.contains("p"))
        {
            qDebug() << QDateTime::currentDateTime().toString("hh:mm:ss") << "<=" << m_hash << result;
        }
        //add 4 bytes header of content size
        QByteArray tosend;
        int size = result.length();
        tosend.reserve(size + 4);

        tosend.append(static_cast<char>(((size >> 24) & 0xFF)));
        tosend.append(static_cast<char>(((size >> 16) & 0xFF)));
        tosend.append(static_cast<char>(((size >> 8) & 0xFF)));
        tosend.append(static_cast<char>((size & 0xFF)));

        tosend.append(result);

        m_bytesSend += write(tosend);
    }
}
