#ifndef CONNECTIONTCP_H
#define CONNECTIONTCP_H

#define MESSAGE_SIZE 4
#define USE_JSON true

#include <QTcpSocket>

#ifdef USE_JSON
#include <QJsonDocument>
#endif


class ConnectionTCP : public QTcpSocket
{
    Q_OBJECT

    int m_messageSize;

    QByteArray m_hash;
    bool m_SocketInited;
    qulonglong m_bytesSend;
    QDataStream m_stream;

    inline bool isInited(){return m_SocketInited;}
    inline void inited(){m_SocketInited = true;}

public:
    explicit ConnectionTCP(int handle, QObject *parent = 0);
    ~ConnectionTCP();

signals:
    void newConnection(ConnectionTCP*);
#ifdef USE_JSON
    void fromInput(QVariantMap data);
#endif
public slots:
    void receiveData();
    void closeSocket();
    void errorSocket(QAbstractSocket::SocketError);
    void send(const QVariantMap&);

};

#endif // CONNECTIONTCP_H
