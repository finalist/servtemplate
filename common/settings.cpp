#include "settings.h"

Settings::Settings(const QString &source, QObject *parent) :
    QSettings(source, QSettings::IniFormat, parent)
{
    if (!contains("port"))
    {
        setValue("port", SERVER_PORT);
    }
    if (!contains("fork"))
    {
        setValue("fork", USE_FORK);
    }
}
