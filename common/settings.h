#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>

#define SERVER_PORT 57055
#define USE_FORK true


class Settings : public QSettings
{
    Q_OBJECT
public:
    explicit Settings(const QString & source, QObject *parent = 0);

signals:

public slots:

};

#endif // SETTINGS_H
