#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>

class Server : public QTcpServer
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = 0);
protected:
    void incomingConnection(int handle);
signals:

public slots:

};

#endif // SERVER_H
