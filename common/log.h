#ifndef LOG_H
#define LOG_H

#include <QObject>
#include <QDebug>


/**
 * @brief The Log class
 *
    qDebug() << "debug";
    qWarning() << "warning";
    qCritical() << "crit";
    qFatal("Fatal");

 */


class Log : public QObject
{
    Q_OBJECT
public:
    explicit Log(QObject *parent = 0);
private:
    static void myMessageHandler(QtMsgType, const QMessageLogContext &, const QString &);

signals:

public slots:

};

#endif // LOG_H
