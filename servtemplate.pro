#-------------------------------------------------
#
# Project created by QtCreator 2014-05-02T13:14:16
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = servtemplate
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    common/settings.cpp \
    common/log.cpp \
    network/connectiontcp.cpp \
    network/server.cpp

HEADERS += \
    common/settings.h \
    common/log.h \
    network/connectiontcp.h \
    network/server.h

OTHER_FILES += \
    README.md
