#include <QCoreApplication>
#include "common/log.h"
#include "common/settings.h"
#include "network/server.h"
#include "unistd.h"

#define APP_NAME "Server"
#define APP_VERSION "0.0.1"
#define INIFILE_LOCATION "/etc/servername/server.ini"


int main(int argc, char *argv[])
{
    Log log;
    QCoreApplication a(argc, argv);
    a.setApplicationName(APP_NAME);
    a.setApplicationVersion(APP_VERSION);

    QString configuration = INIFILE_LOCATION;
    if (argc == 2)
    {
        configuration = argv[1];
    }
    else
    {
        return -1;
    }

    Settings settings(configuration);

    quint16 port  = static_cast<quint16>(settings.value("port").toUInt());
    bool bFork = settings.value("fork").toBool();

    if (bFork)
    {
        pid_t pid = 0;
        pid=fork();
        if (pid == -1)
        {
            return EXIT_FAILURE;
        }
        else if (pid > 0)
        {
            return EXIT_SUCCESS;
        }
    }

    qsrand(time(NULL));

    Server server(&a);
    if (false == server.startServer(port))
    {
        qCritical() << "cant listen port: " << server.errorString();
    }
    return a.exec();
}
